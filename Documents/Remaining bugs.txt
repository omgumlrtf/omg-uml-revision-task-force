Remaining problems

1. Still plenty of fuzzy PNG bitmaps that need redrawing/re-exporting from source tools.

2. Some of the SVGs that we do have have clipped label text. Bug in SVG exporters - so need re-exporting from tools and/or manual editing in Inkscape.

3. Some links have underlining that extends beyond the end of the text. Looks like a LibreOffice bug - it's underlining the closing line end (e.g. under 9.10.17.2).

4. One set of facing page preface footer text gets lost when ever the file is loaded. Must be manually restored before printing. Definitely a bug in LibreOffice.