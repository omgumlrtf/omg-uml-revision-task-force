# Quick script to find what text:list directives wrap headings

/^[ \t]*<text:list xml:id=/     { LastListDir = $0
                                  Loc = match($0, /text:style-name="[^"]+"/)
                                  ListTag = substr($0, RSTART+17, RLENGTH-18)
                                  if (ListLevel !=0)
                                    { print "***** Nested tagged list!"
                                      exit
                                    }
                                  ListLevel++
                                  ListTagsUsed[ListTag]++
                                  next
                                }

/^[ \t]*<text:list>$/           { if (ListLevel == 0)
                                    { print "****** Untagged list at level 0!"
                                      exit
                                     }
                                  ListLevel++
                                  next
                                }

/^[ \t]*<\/text:list>$/         { if (ListLevel == 0)
                                    { print "******** List end directive at level 0!"
                                      exit
                                    }
                                 ListLevel--
                                 if (ListLevel == 0)
                                   { ListTag = ""
                                   }
                                 next
                               }
                               
/^[ \t]*<text:list-item>$/    { next }

/^[ \t]*<\/text:list-item>$/  { next }

/^[ \t]*<text:h /        { L = match($0, /text:outline-level="[^"]+"/)
                           Level = substr($0, RSTART+20, RLENGTH-21)

                           print "Heading: " $0 "\nList Level: " ListLevel " Tag: " ListTag "\n"
                           if (ListLevel!=0) {HeadingListTagsUsed[ListTag]++}
                           next
                        }


# Catch-all looks for dirctives inside tagged lists that aren't headings, and bumps use count for them,
# so that we can see if any heading-related tags are ever used outside headings

                        { if (ListLevel!=0) {NonHeadingListTagsUsed[ListTag]++ }
                        }
                        
END   { for (i in HeadingListTagsUsed)
          { print "HeadingListTagsUsed[\"" i "\"] = " HeadingListTagsUsed[i]
            if (i in NonHeadingListTagsUsed) {print "*** OVERLAP!!!"}
          }
          
        print "\n----------------------------------------\n"
        
        for (i in NonHeadingListTagsUsed)  
          { print "NonHeadingListTagsUsed[\"" i "\"] = " NonHeadingListTagsUsed[i]
            if (i in HeadingListTagsUsed) {print "*** OVERLAP!!!"}

          }
      }
      
      
      
      
