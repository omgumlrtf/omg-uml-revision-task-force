# Script to input tab-separated list of figures, output HTML table

BEGIN { FS = "\t"
        stem = "file:///Users/ajw/Documents/OMG/By%20specification/UML/UML%202.5%20Sources/Source%20with%20Diagrams/Specification_test"
        print "<table style=\"width:100%\">"
      }


$2 ~ /.svg/        { print "<tr>"
                     print "<td><a href=\"file://" stem substr($2, 2) "\">" $2 "</a></td>"
                     print "<td><a href=\"file://" stem substr($2, 2, length($2)-5) ".png\">PNG Alt</a></td>"
                     print "<td>" $3 "</td>"
                     print "<td>" $4 "</td>"
                     print "</tr>"
                     next
                   }
                   
                   { print "<tr>"
                     print "<td><a href=\"file://" stem substr($2, 2) "\">" $2 "</a></td>"
                     print "<td></td>"
                     print "<td>" $3 "</td>"
                     print "<td>" $4 "</td>"
                     print "</tr>"
                     next
                   }

END { print "</table>"
    }
    