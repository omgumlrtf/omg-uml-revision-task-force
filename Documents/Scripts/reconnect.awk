# Script to reconnect UML 2.5 graphics with 


BEGIN  {  FindCmd = "find . -print"
          while ((FindCmd | getline x) > 0)
            { # print x > "Debug.txt"
              gsub(/&/, "\\&amp;", x)   # Fix any ampersands, ready for XML
              # print x > "Debug.txt"
              n = split(x, Parts, "/")  # Split into path components

              # print n " " Parts[n-1] " " Parts[n] > "Debug.txt"

              FileName = Parts[n]       # File name is last part

              if (substr(FileName, 1, 1) == ".") continue  # Ignore hidden files

              if (FileName in Files)    # Did we already see this name?
                { print "*** ERROR: Filename " Filename " duplicated:" | "cat 1>&2"
                  print Files[FileName] | "cat 1>&2"
                  print x
                  exit
                }
              else
                { Files[FileName] = x
                  printf "%50s  --> %s\n", FileName, x > "Graphics_locations.txt"
                }
            }
          close(FindCmd)
       }


/<draw:image.*\/><svg:desc>/  { D_loc = match($0, /<svg:desc>.+<\/svg:desc>/)  # Find text between desc tags
                             # print "D_loc: " D_loc " Length: " length($0) " RLENGTH: " RLENGTH " RSTART+RLENGTH " RSTART+RLENGTH
                             if (length($0) != RSTART+RLENGTH-1)
                               { print "*** ERROR: Desc not at End of line!" | "cat 1>&2"   # Complain if it's not at line end
                                 exit
                               }
                             Found++
                             Desc = substr($0, RSTART+10, RLENGTH-21)  # Pull out text between desc tags
                             # print $0 "\n" Desc "\n\n"
                             
                             match("\\" Desc, /[^\\]+$/)               # Split off the part after the "\" (if any)
                             LastComp = substr(Desc, RSTART-1, RLENGTH)
                             # print $0 "\n" LastComp "\n\n"
                             
                             if (LastComp in Files)                    # Do we have it?
                               { Substitute = Files[LastComp]          # Make it the candidate substitution
                               
                                 # If this is *not* a .svg or .emf, check to see if we have a corresponding .svg or .emf
                                 
                                 if (tolower(LastComp) ~ /\.((png)|(gif)|(jpg))$/)
                                   { SVGCandidate = LastComp
                                     sub(/\.[^.]+$/, ".svg", SVGCandidate)
                                     EMFCandidate = LastComp
                                     sub(/\.[^.]+$/, ".emf", EMFCandidate)
                                     
                                     print "SVGCandidate: " SVGCandidate "  EMFCandidate: " EMFCandidate > "Debug.txt"
                                     
                                     if (SVGCandidate in Files)
                                       { print "Substituting " Files[SVGCandidate] " for " Substitute > "Debug.txt"
                                         Substitute = Files[SVGCandidate]
                                         SN++
                                       }
                                     else
                                       { if (EMFCandidate in Files)
                                           { print "Substituting " Files[EMFCandidate] " for " Substitute > "Debug.txt"
                                             Substitute = Files[EMFCandidate]
                                             SN++
                                           }
                                         else
                                           { print "No vector substitute available for " Substitute > "Debug.txt"
                                             Bitmap++
                                           }  
                                       }
                                   }
                                 else
                                   { print "No substitution attempted for (vector format file?) " Substitute > "Debug.txt"
                                   }
                               
                                 New = $0
                                 
                                 # In order to prevent any & characters in Substitute messing things up, we have to
                                 # put \ in front of each to escape them. This may not be portable between
                                 # AWK implementations. See "GAWK: Effective AWK Programming" section 8.1.3.1.
                                 
                                 gsub(/&/, "\\\\&", Substitute)
                                 
                                 sub(/<draw:image.*\/>/, "<draw:image xlink:href=\"" Substitute "\" xlink:type=\"simple\" xlink:show=\"embed\" xlink:actuate=\"onLoad\"/>", New)
                                 print New
                                 print "Old: " $0 "\nNew: " New "\n---------------------------------------------------------------------------------------------" > "Debug.txt"
                               }
                             else
                               { print "*** ERROR: No file for: " LastComp | "cat 1>&2"
                                 exit
                               }
                             next
                           }
                           
 						   { print   # Default action is pass-through
 						   }

END                        { print "Ended with " Found " diagrams inserted. " SN " Vector substitutions made. " Bitmap " bitmap images remain." | "cat 1>&2"
 						   }