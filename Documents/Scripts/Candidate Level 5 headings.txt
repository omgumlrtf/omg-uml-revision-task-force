21>22 changed these lines to Hdg 5"

   <text:p text:style-name="P25">Kinds of States</text:p>
   <text:p text:style-name="P25">State configurations</text:p>
   <text:p text:style-name="P25">State entry, exit, and doActivity Behaviors</text:p>
   <text:p text:style-name="P25">State history</text:p>
   <text:p text:style-name="P25">Entering a State</text:p>
   <text:p text:style-name="P25">Exiting a State </text:p>
   <text:p text:style-name="P25">Submachine States and submachines</text:p>
   <text:p text:style-name="P25">High-level (group) Transitions</text:p>
   <text:p text:style-name="P25">Completion Transitions and completion events</text:p>
   <text:p text:style-name="P25">Compound transitions</text:p>
   <text:p text:style-name="P25">Transition ownership</text:p>
   <text:p text:style-name="P25">The run-to-completion paradigm</text:p>
   <text:p text:style-name="P25"><text:soft-page-break/>Enabled Transitions </text:p>
   <text:p text:style-name="P25">Firing priorities</text:p>
   <text:p text:style-name="P25">Transition selection algorithm </text:p>
   <text:p text:style-name="P25">Transition execution sequence </text:p>
   <text:p text:style-name="P25">Composite State</text:p>
   <text:p text:style-name="P25">Submachine State </text:p>
   <text:p text:style-name="P25">State list notation</text:p>
   <text:p text:style-name="P25">Signal receipt symbol</text:p>
   <text:p text:style-name="P25">Signal send symbol</text:p>
   <text:p text:style-name="P25">Choice point symbol</text:p>
   <text:p text:style-name="P25">Merge symbol</text:p>
   <text:p text:style-name="P25">Deferred triggers </text:p>
   <text:p text:style-name="P25">Transition redefinition </text:p>
   <text:p text:style-name="P25">Unexpected trigger reception </text:p>
   <text:p text:style-name="P25">Unexpected Behavior </text:p>
   <text:p text:style-name="P25">Equivalences to pre- and post-conditions of operations </text:p>
   <text:p text:style-name="P25"><text:soft-page-break/>Using other types of Events in ProtocolStateMachines </text:p>
   <text:p text:style-name="P44">Transition kinds relative to source<text:tab/><text:tab/></text:p>
   <text:p text:style-name="P44">Conflicting Transitions</text:p>
   <text:p text:style-name="P44">Action symbols</text:p>
   <text:p text:style-name="P44">State redefinition</text:p>
   <text:p text:style-name="P43">MOF-Equivalent Semantics</text:p>
   <text:p text:style-name="P23">Restricting Availability of UML Elements</text:p>
   <text:p text:style-name="P23"><text:soft-page-break/>Style Guidelines </text:p>
   <text:p text:style-name="P24">Integrating and Extending Profiles</text:p>

Used this grep pattern:

<text:p text:style-name="(P23|P24|P25|P43|P44)">(<text:soft-page-break/>)?([^x][-A-Za-z (),]+)</text:p>$
<text:h text:style-name="Heading_20_5" text:outline-level="5">\3</text:h>