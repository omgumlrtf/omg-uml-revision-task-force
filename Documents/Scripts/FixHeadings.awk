# Script to unwrap headings from enclosing lists, and give them heading style

BEGIN { # Initialise array of tags for lists that wrap headings. We know that the only
        # text inside lists with these tags are heading directives (or non-tagged lists)

        HeadingListTagsUsed["WWNum1126"] = 212
        HeadingListTagsUsed["WWNum1093"] = 5
        HeadingListTagsUsed["WWNum461"] = 14
        HeadingListTagsUsed["WWNum1094"] = 4
        HeadingListTagsUsed["WWNum590"] = 254
        HeadingListTagsUsed["WWNum465"] = 101
        # HeadingListTagsUsed["WWNum1098"] = 23  No - this is annex B
        HeadingListTagsUsed["WWNum678"] = 745
        HeadingListTagsUsed["WWNum16"] = 37
        HeadingListTagsUsed["WWNum517"] = 187
        HeadingListTagsUsed["WWNum19"] = 274
        HeadingListTagsUsed["WWNum1034"] = 7
        HeadingListTagsUsed["WWNum934"] = 54
        HeadingListTagsUsed["WWNum1080"] = 38
        HeadingListTagsUsed["WWNum1035"] = 55
        HeadingListTagsUsed["WWNum323"] = 209
        HeadingListTagsUsed["WWNum167"] = 43
        HeadingListTagsUsed["WWNum951"] = 261
        HeadingListTagsUsed["WWNum582"] = 32
        HeadingListTagsUsed["WWNum2"] = 22
        HeadingListTagsUsed["WWNum1053"] = 20
        HeadingListTagsUsed["WWNum1054"] = 88
        HeadingListTagsUsed["WWNum666"] = 74
        HeadingListTagsUsed["WWNum180"] = 289
        HeadingListTagsUsed["WWNum424"] = 105
        HeadingListTagsUsed["WWNum104"] = 25
        HeadingListTagsUsed["WWNum396"] = 14
        HeadingListTagsUsed["WWNum314"] = 38
        HeadingListTagsUsed["WWNum107"] = 203
        HeadingListTagsUsed["WWNum1077"] = 7
        HeadingListTagsUsed["WWNum283"] = 20
        HeadingListTagsUsed["WWNum284"] = 104
        HeadingListTagsUsed["WWNum493"] = 19
        # HeadingListTagsUsed["WWNum1206"] = 5  No - this is annex E
      }


/^[ \t]*<text:list xml:id=/     { Loc = match($0, /text:style-name="[^"]+"/)
                                  ListTag = substr($0, RSTART+17, RLENGTH-18)
                                  if (ListLevel !=0)
                                    { print "***** ERROR: Nested tagged list!" | "cat 1>&2"
                                      exit
                                    }
                                  ListLevel++
                                  if (ListTag in HeadingListTagsUsed)
                                    { InHeadingList = 1
                                      
                                      I = match($0, /</)     # Grab the indent level
                                      Indent = substr($0, 1, I-1)
                                    }
                                  else
                                    { print $0       # Pass through all the non-heading ones
                                      InHeadingList = 0
                                    }
                                  next
                                }

/^[ \t]*<text:list>$/           { if (ListLevel == 0)
                                    { print "****** ERROR: Untagged list at level 0!" | "cat 1>&2"
                                      exit
                                     }
                                  if (InHeadingList == 0)
                                    { print $0     # Pass through all the non-heading ones
                                    }
                                  ListLevel++
                                  next
                                }

/^[ \t]*<\/text:list>$/         { if (ListLevel == 0)
                                    { print "******** ERROR: List end directive at level 0!" | "cat 1>&2"
                                      exit
                                    }
                                 if (InHeadingList == 0)
                                    { print $0     # Pass through all the non-heading ones
                                    }                                    
                                 ListLevel--
                                 if (ListLevel == 0)
                                   { ListTag = ""
                                     InHeadingList = 0
                                   }
                                 next
                               }
                               
/^[ \t]*<text:list-item>$/    { if (InHeadingList == 0)
                                  { print $0     # Pass through all the non-heading ones
                                  }                                    
                                next
                              }

/^[ \t]*<\/text:list-item>$/  { if (InHeadingList == 0)
                                  { print $0     # Pass through all the non-heading ones
                                  }                                    
                                next
                              }

# When we find a heading, re-write the style-name to correspond to the outline level
# If we're inside a heading list, outdent it just for neatness.

/^[ \t]*<text:h /        { L = match($0, /text:outline-level="[^"]+"/)
                           Level = substr($0, RSTART+20, RLENGTH-21)
                           Old = $0
                           
                           sub(/<text:h text:style-name="[^"]+"/, "<text:h text:style-name=\"Heading_20_" Level "\"", $0)
                           
                           if (InHeadingList != 0)
                             { sub(/^[ \t]*/, Indent, $0)
                             }
                             
                           print "Level: " Level "\nOld:\n" Old "\nNew:\n" $0 "\n-------------------" > "replacements.txt"

                           print $0
                           
                           if (InHeadingList != 0)
                             { HeadingListTagsUsed[ListTag]--           # Note that we found it
                             }                                    
 
                           next
                        }


# Catch-all looks for dirctives inside tagged lists that aren't headings or list things,
# flgs error if it finds one, else just passes throug

                        { if (InHeadingList != 0)
                            { print "******** ERROR: Unexpected line in HeadingList: " $0 | "cat 1>&2"
                              exit
                            }
                          print $0
                        }
                        
END   { for (i in HeadingListTagsUsed)
          { if (HeadingListTagsUsed[i] != 0)
              { print "******** ERROR: Heading tag: " i " has " HeadingListTagsUsed[i] " error" | "cat 1>&2"
              }
          }
      }
