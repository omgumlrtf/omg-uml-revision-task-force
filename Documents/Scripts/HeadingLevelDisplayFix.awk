# Program to find all WWNum list definitions associcated with headings and set all to display
# 5 levels

function FixLevels()
  { # First, fix any style:num-format=""
  
    sub(/style:num-format=""/, "style:num-format=\"1\"")

    # Now fix the display levels. Unrolled loop for speed
    
    sub(/text:level="2" style:num-format="1"/, "text:level=\"2\" style:num-format=\"1\" text:display-levels=\"2\"")
    sub(/text:level="3" style:num-format="1"/, "text:level=\"3\" style:num-format=\"1\" text:display-levels=\"3\"")
    sub(/text:level="4" style:num-format="1"/, "text:level=\"4\" style:num-format=\"1\" text:display-levels=\"4\"")
    sub(/text:level="5" style:num-format="1"/, "text:level=\"5\" style:num-format=\"1\" text:display-levels=\"5\"")

    print $0

  }


/^[ \t]*<text:list-style style:name="WWNum1126">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1093">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum461">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1094">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum590">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum465">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1098">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum678">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum16">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum517">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum19">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1034">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum934">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1080">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1035">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum323">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum167">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum951">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum582">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum2">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1053">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1054">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum666">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum180">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum424">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum104">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum396">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum314">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum107">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1077">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum283">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum284">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum493">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }
/^[ \t]*<text:list-style style:name="WWNum1206">$/,/^[ \t]*<\/text:list-style>/ { FixLevels() ; next }

	{ print $0 }