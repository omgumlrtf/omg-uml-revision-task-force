# README #

Repository for the Unified Modeling Language (UML) specification
published by the Object Management Group (OMG) UML Revision Task Force (RTF).

Each UML revision corresponds to a branch of this repository.

### [UML 2.5](http://www.omg.org/spec/UML/2.5) ###

Branch: UML2.5

### UML 2.5.1 (Urgent Revision)

 